.. _installation:

Using pip
=========

.. code:: bash

    pip install ipyd3
    jupyter nbextension enable --py --sys-prefix ipyd3  # can be skipped for notebook 5.3 and above


JupyterLab extension
====================

If you have JupyterLab, you will also need to install the JupyterLab extension:

.. code:: bash

    jupyter labextension install ipyd3

Development installation
========================

For a development installation (requires npm):

.. code:: bash

    git clone https://gitlab.com/teia_engineering/ipyd3.git
    cd ipyd3
    pip install -e .
    jupyter nbextension install --py --symlink --sys-prefix ipyd3
    jupyter nbextension enable --py --sys-prefix ipyd3
    jupyter labextension install ipyd3 # If you are developing on JupyterLab


For automatically building the JavaScript code every time there is a change, run the following command from the ``ipyleaflet/js/`` directory:

.. code:: bash

    npm run watch

If you are on JupyterLab you also need to run the following in a separate terminal:

.. code:: bash

    jupyter lab --watch

Every time a JavaScript build has terminated you need to refresh the Notebook page in order to load the JavaScript code again.
