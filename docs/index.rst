.. ipyd3 documentation master file, created by
   sphinx-quickstart on Thu Jul 23 17:54:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ipyd3's documentation!
=================================
ipyd3 is a library that attemps to wrap the javascript library `D3.js <https://d3js.org>`_
in a Jupyter Widget. D3.js is a JavaScript library for manipulating documents based on data.
D3 helps you bring data to life using HTML, SVG, and CSS.

Right now we have only implemented the Force Directed Graph and the Hierarchical Graph 
functionality of the D3.js library. If you would like to add another functionality 
please add a merge request with your source code and we will expand the libary!

.. image:: initial_graph.png
  :width: 900
  :alt: graph image

.. toctree::
    :caption: Installation
    :maxdepth: 4

    installation

.. toctree::
    :caption: Force Directed Graph

    forcedirectedgraph

.. toctree::
    :caption: Hierarchical Graph (beta)

    hierarchicalGraph
