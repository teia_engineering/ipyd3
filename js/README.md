Library for visualizing D3.js inside Jupyter.

Package Install
---------------

**Prerequisites**
- [node](http://nodejs.org/)

```bash
npm install --save ipyd3
```
